# Machine Learning in structures with Lithium content

Based on chemical and physical features, we deploy a decision tree classifier to predict the crystal system of materials containing Li. We also design a deep neural network for the prediction of the band energy.
